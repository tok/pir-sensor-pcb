

const int sensorPin = A3;
const int ledPin = 0;
const int outputPin = 1;
const int chainInPin = 2;

#define IIR_ORDER  6

// from scipy import signal
// signal.iirfilter(2, [0.01, 0.05], ftype='butter', analog=False)
// signal.iirfilter(2, [0.002, 0.2], rs=60, ftype='cheby2', analog=False)
const double iirCoeffA[IIR_ORDER+1] = {  
           0.10910486, -0.02190101, -0.17439951, -0.02190101,  0.10910486
       };
const double iirCoeffB[IIR_ORDER] = {
          -2.85366542,  3.05754246, -1.52653633,  0.32347835
};


double iirZ[IIR_ORDER] = {0};  // state


double IIR_ProcessSample(double sample) {
  
  double feedbackSum = sample;
  for(int i = 0; i < IIR_ORDER; ++i) {
    feedbackSum -= iirCoeffB[i] * iirZ[i];
  }
  
  double forwardSum = feedbackSum * iirCoeffA[0];
  
  for(int i = 0; i < IIR_ORDER; ++i) {
    forwardSum += iirCoeffA[i+1] * iirZ[i];
  }
  
  // shift state, push new sample
  for(int i = IIR_ORDER-1; i > 0; --i) {
    iirZ[i] = iirZ[i-1];
  }
  iirZ[0] = feedbackSum;
  
  
  return forwardSum;
}

double threshold = 0;

#define NUM_LEARN_SAMPLES    16
#define SAMPLE_DELAY         5

static uint16_t countSmallerNumbers(double ref, const double* arr, uint16_t len) {
  uint16_t sum = 0;
  for(uint16_t i = 0; i < len; ++i) {
    sum += (arr[i] < ref) ? 1 : 0;
  }
  return sum;
}

// the setup routine runs once when you press reset:
void setup() {
  //Serial.begin(9600);
  
  pinMode(chainInPin, INPUT_PULLUP);
  
  pinMode(outputPin, OUTPUT);
  digitalWrite(outputPin, 0);
  
  pinMode(ledPin, OUTPUT);
  
  for(int i = 0; i < 4; ++i) {
  digitalWrite(ledPin, 0);
  delay(500);
  digitalWrite(ledPin, 1);
  delay(500);
  }
  
  double learnSamples[NUM_LEARN_SAMPLES] = {0};
  
  for(int s = 0; s < NUM_LEARN_SAMPLES; ++s) {
    
    double filteredMax = 0;
  
    for(int i = 0 ; i < (1L<<6); ++i) {
      int sensorValue = analogRead(sensorPin);
      double filtered = IIR_ProcessSample(sensorValue);
      
      if(i == 0) {
        filteredMax = abs(filtered);
      }
      
      filteredMax = max(filteredMax, abs(filtered));
      delay(SAMPLE_DELAY);
    }
    learnSamples[s] = filteredMax;
  }
  
  // Calculate median
  double median = learnSamples[0];
  for(int i = 0; i < NUM_LEARN_SAMPLES; ++i) {
    if(countSmallerNumbers(learnSamples[i], &learnSamples[0], NUM_LEARN_SAMPLES) == NUM_LEARN_SAMPLES/2) {
      median = learnSamples[i];
      break;
    }
  }
  
  
  threshold = 1.01*median;
  
  digitalWrite(ledPin, 0);
  delay(500);
}

const uint8_t outputBreakTime = 100;
const uint8_t outputHighTime = 100;
const uint8_t outputStartTime = outputBreakTime + outputHighTime;
uint8_t outputCountdown = 0;

typedef enum {
  CHAIN_STATE_WAIT_FOR_RISING,
  CHAIN_STATE_WAIT_FOR_FALLING,
} chain_state_t;

uint8_t lastChainIn = 1;

const uint16_t maxChainInTime = 10*outputHighTime;
uint16_t chainInHighCount = 0;

const double secondOrderThreshold = 0.5;
double secondOrderValue = 0;

void loop() {
  
  double filtered = 0;
  //for(int i = 0 ; i <32; ++i) {
    int sensorValue = analogRead(sensorPin);
    filtered = IIR_ProcessSample(sensorValue);
    //filtered += 0.2*(sensorValue-filtered);
    //analogWrite(pwmPin, (int)(min(255, max(0, 16-filtered))));
    delay(SAMPLE_DELAY);
    
    const int rectified = abs(filtered) > threshold;
    //digitalWrite(ledPin, rectified);
    secondOrderValue += 0.1*(rectified-secondOrderValue);

    const uint8_t chainIn = digitalRead(chainInPin);
    
    if(chainInHighCount > 0 || (outputCountdown == 0 && lastChainIn == 0 && chainIn == 1)) {
      chainInHighCount++;
      chainInHighCount %= maxChainInTime;
      outputCountdown = outputStartTime;
      
      if(chainIn == 0) {
        // Falling edge
        chainInHighCount = 0;
      }
    }
    
    if(outputCountdown == 0) {
      if(secondOrderValue > secondOrderThreshold) {
        // Triger output.
        outputCountdown = outputStartTime;
      }
    }
    lastChainIn = chainIn;
  //}
  
  digitalWrite(ledPin, outputCountdown > outputBreakTime);
  digitalWrite(outputPin, outputCountdown > outputBreakTime);
  outputCountdown = max(0, outputCountdown-1);
}



