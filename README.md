# Passive Infra-Red (PIR) Motion Detector Sensor Module

A sensor module based on a pyro-electric PIR sensor, an op-amp and a Attiny85 micro controller.


## LICENCE
PCB and firmware are licensed under the CERN OHL-S v2.
